"use strict";

function escapeHtmlEntities (str) {
  if (typeof jQuery !== 'undefined') {
    // Create an empty div to use as a container,
    // then put the raw text in and get the HTML
    // equivalent out.
    return jQuery('<div/>').text(str).html();
  }

  // No jQuery, so use string replace.
  return str
    .replace(/&/g, '&amp;')
    .replace(/>/g, '&gt;')
    .replace(/</g, '&lt;')
    .replace(/"/g, '&quot;')
    .replace(/'/g, '&apos;');
}

$(function($) {

	var
		SITE = window.SITE || {},
		base, cpf;

	var fullPath = window.location.host + "" + window.location.pathname;
	var currentPath = location.toString().substr(this.baseLength).split('/');
	var currentState = currentPath.slice(-1).pop();


	if (window.location.host == 'localhost')
		base = '';
	else
		base = window.location.host;

	SITE.init = function() {

		$('.equalize').equalize();

		$(".expand").on( "click", function(e) {
			e.preventDefault();

			$(this).next().slideToggle(200);
			$expand = $(this).find(">:first-child");

			if($expand.html() == '<i class="fa fa-chevron-down"></i>') {
			  $expand.html('<i class="fa fa-chevron-up"></i>');
			} else {
			  $expand.html('<i class="fa fa-chevron-down"></i>');
			}
		});

	},


	SITE.slideConfiar = function() {

		$('.confiar_slide').owlCarousel({
			dots: true,
			nav: true,
			loop: true,
			items: 1,
			navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
			responsive: {
				0: {
					items: 1,
					nav: false
				},
				800: {
					items: 1
				}
			}
		});

	},

	SITE.mobileTitens = function() {

		$('.titens_slide').owlCarousel({
			dots: true,
			nav: false,
			autoHeight: true,
			loop: false,
			items: 1,
			navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
			responsive: {
				0: {
					items: 1
				},
				800: {
					items: 1
				}
			}
		});

	},

	SITE.discountForm = function() {


		//close on click outside
		var mouse_is_inside = false;

		//OPEN
		$(".btn-discount").on( "click", function(e) {

			e.preventDefault();
			$("#form-discount").css({'top':0}).addClass('active');
			//$("html").toggleClass('overHidden');


				$('#form-discount').hover(function(){
					mouse_is_inside=true;
				}, function(){
					mouse_is_inside=false;
				});

				$("body").mouseup(function(){
					if(!mouse_is_inside) $("#form-discount").css({'top':'-101vh'});
				});


		});

		//CLOSe
		$("#closediscount").on( "click", function(e) {

			e.preventDefault();
			//$("html").toggleClass('overHidden');
			$("#form-discount").css({'top':'-101vh'}).removeClass('active');
			mouse_is_inside = false;

		});


		// jQuery.validator.addMethod("cep", function(value, element) {
		//   return this.optional(element) || /^[4]{1}[0-9]{1}[0-9]{3}-[0-9]{3}$/.test(value);
		// }, "Desculpe, nossos serviços ainda não estão disponíveis na sua região. Vamos avisar assim que chegarmos. ;)");


	// var $formDiscount = $('#discount__form');

	// $formDiscount.validate({
	// 	rules: {
	// 		d_cep: {
	// 			required: true
	// 		},
	// 		d_email: {
	// 			required: true,
	// 			email: true
	// 		},
	// 		d_email2: {
	// 			required: true,
	// 			equalTo: '#d_email'
	// 		}
	// 	},
	// 	submitHandler: function() {

	// 		$formDiscount.find('#submit_btn')
	// 		.css('pointer-events', 'none')
	// 		.html('Enviando...');

	// 		$formDiscount.ajaxChimp({
	//             language: 'cm',
	//             url: '//sejanova.us17.list-manage.com/subscribe/post?u=9b7742f6d95343c60b92b76a3&amp;id=138ca71499',
	//             callback: function(resp) {
 //                if (resp.result == 'success') {


 //                    $formDiscount.find('.fields-content>.row').hide();
	// 				$formDiscount.find('.successdiscount').removeClass('hide');
	// 				$formDiscount.find('#submit_btn').css('pointer-events', 'inherit').html('FECHAR');

	// 				$("#submit_btn").on( "click", function(e) {

	// 					e.preventDefault();
	// 					$("#form-discount").css('top','-100%').removeClass('active');

	// 				});

	// 				$formDiscount.find('input[type=text],input[type=tel],input[type=email],textarea').val('');

	//                 } else {
	//                 	$formDiscount.find('#submit_btn').css('pointer-events', 'inherit').html('ENVIAR');
	//                 }



	//             }
	//         });


	// 	},
	// 	messages: {
	// 		d_email: {
	// 			required: "Seu e-mail está correto?"
	// 		},
	// 		d_email2: {
	// 			required: "Informe um email válido"
	// 		}
	// 	},
	// 	errorPlacement: function($error, $element) {
	// 		var name = $element.attr("name");
	// 		$("#error" + name).append($error);
	// 		//$("#error" + name).parent('div').find('.input-effect').addClass('p-error');
	// 	},
	// 	highlight: function(element) {
	// 		$(element).removeClass('valid');
	// 		$(element).parent('div').addClass('p-error');
	// 	},
	// 	unhighlight: function(element) {
	// 		$(element).addClass('valid');
	// 		$(element).parent('div').removeClass('p-error');
	// 	}
	// });




//       $.ajaxChimp.translations.cm = {
//           'submit': '...',
//           0: ' ',
//           1: 'E-mail inválido.',
//           2: 'E-mail inválido.',
//           3: 'The domain portion of the email address is invalid (the portion after the @: )',
//           4: 'The username portion of the email address is invalid (the portion before the @: )',
//           5: 'O email parece invÃ¡lido. Entre com um email diferente!'
//       }



		var $formDiscount = $('#discount__form');
		$formDiscount.validate({
			focusInvalid: false,
			rules: {
				d_cep: {
					required: true
				},
				d_email: {
					required: true,
					email: true
				},
				d_email2: {
					required: true,
					equalTo: '#d_email'
				}
			},
			submitHandler: function() {

				$('#submit_btn')
					.css('pointer-events', 'none')
					.html('Enviando...');

				var dataString = $formDiscount.serialize();




				$.ajax({
					url: BASE+'inicio/discount',
					type: 'POST',
					data: dataString,
					error: function(data) {

						$formDiscount.find('#obrigadocontato').addClass('alert alert-danger').fadeIn().slideDown().html('POR FAVOR, TENTE NOVAMENTE!');
						$formDiscount.find('#submit_btn').removeAttr('disabled').html('ENVIAR');

					},
					success: function(data) {

						if (data.error) {

							//$formDiscount.find('#obrigadocontato').addClass('alert alert-danger').fadeIn().slideDown().html(data.error);
							$formDiscount.find('#submit_btn').removeAttr('disabled').css('pointer-events', 'inherit').html('ENVIAR');

						} else {

							$formDiscount.find('.fields-content>.row').hide();
							$formDiscount.find('.successdiscount').removeClass('hide');
							$formDiscount.find('#submit_btn').removeAttr('disabled').css('pointer-events', 'inherit').html('FECHAR');

							$("#submit_btn").on( "click", function(e) {

								e.preventDefault();
								$("#form-discount").css('top','-100%').removeClass('active');

							});

							$formDiscount.find('input[type=text],input[type=tel],input[type=email],textarea').val('');

						}


					}
				});
			},
			messages: {
				d_email: {
					required: "Seu e-mail está correto?"
				},
				d_email2: {
					required: "Informe um email válido"
				}
			},
			errorPlacement: function($error, $element) {
				var name = $element.attr("name");
				$("#error" + name).append($error);
				//$("#error" + name).parent('div').find('.input-effect').addClass('p-error');
			},
			highlight: function(element) {
				$(element).removeClass('valid');
				$(element).parent('div').addClass('p-error');
			},
			unhighlight: function(element) {
				$(element).addClass('valid');
				$(element).parent('div').removeClass('p-error');
			}
		});

	},



	SITE.slideTestimonial = function() {

		$('.testimonial-style3').owlCarousel({
			loop: true,
			nav: true,
			margin: 210,
			center: false,
			dots: false,
			navText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
			responsive: {
				0: {
					items: 1,
					margin: 15,
					nav:false,
					dots: true
				},
				1024: {
					items: 1,
					margin: 15,
					nav:false,
					dots: true
				},
				1200: {
					items: 1.3,
					margin: 210
				},
				1600: {
					items: 1.7,
					margin: 210
				}
			}
		});

	},


	SITE.medias = function() {

		$('.popup-youtube, .popup-vimeo, .popup-gmaps').magnificPopup({
			disableOn: 700,
			type: 'iframe',
			mainClass: 'mfp-fade',
			removalDelay: 160,
			preloader: false,
			fixedContentPos: true
		});


		$('.lightbox-gallery').magnificPopup({
			delegate: 'a',
			type: 'image',
			closeOnContentClick: true,
			closeBtnInside: false,
			midClick: true,
			tLoading: 'Loading image #%curr%...',
			mainClass: 'mfp-fade',
			gallery: {
				enabled: true,
				navigateByImgClick: true,
				preload: [0, 1] // Will preload 0 - before current, and 1 after the current image
			},
			image: {
				tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
				titleSrc: function(item) {
					return item.el.attr('title');
				}
			},
			callbacks: {
				open: function() {
					$.magnificPopup.instance.close = function() {
						if (!device.mobile()) {
							$.magnificPopup.proto.close.call(this);
						} else {
							$('button.mfp-close').click(function() {
								$.magnificPopup.proto.close.call(this);
							});
						}
					}
				}
			}
		});

		$(document).on('click', '.inlineOpen', function (e) {
			e.preventDefault();
			 var itemSrc = $(this).attr('href')

			$.magnificPopup.open({
				 items: {
					src: itemSrc,
				},
				type: 'inline',
				closeBtnInside: false,
				showCloseBtn: false,
				midClick: true,
				overflowY: 'auto',
				removalDelay: 160,
				mainClass: 'mfp-fade',
				callbacks: {
					open: function() {
						$.magnificPopup.instance.close = function() {
							if (!device.mobile()) {
								$.magnificPopup.proto.close.call(this);
							} else {
								$('button.mfp-close').click(function() {
									$.magnificPopup.proto.close.call(this);
								});
							}
						}
					}
				}
			});
		});

	},


	SITE.menuHeader = function() {

		// $(window).scroll(function() {

		// 	var shrink_header = $('.shrink-header').length;
		// 	var shrink_medium_header = $('.shrink-medium-header').length;
		// 	var shrink_big_header = $('.shrink-big-header').length;
		// 	var shrink_transparent_header_light = $('.shrink-transparent-header-light').length;
		// 	var shrink_transparent_header_dark = $('.shrink-transparent-header-dark').length;
		// 	if (shrink_medium_header) {
		// 		var windowsize = $(window).width();
		// 		if (windowsize <= 991 && windowsize == 768) {
		// 			var header_offset = -106;
		// 		} else if (windowsize <= 767) {
		// 			var header_offset = -90;
		// 		} else {
		// 			var header_offset = -110;
		// 		}

		// 	} else if (shrink_big_header) {
		// 		var windowsize = $(window).width();
		// 		if (windowsize <= 991) {
		// 			var header_offset = -64;
		// 		} else {
		// 			var header_offset = -115;
		// 		}

		// 	} else if (shrink_header || shrink_transparent_header_light || shrink_transparent_header_dark) {
		// 		var windowsize = $(window).width();
		// 		if (windowsize <= 991 && windowsize == 768) {
		// 			var header_offset = -64;
		// 		} else if (windowsize <= 767) {
		// 			var header_offset = -60;
		// 		} else {
		// 			var header_offset = -68;
		// 		}

		// 	} else {
		// 		var header_offset = 1;
		// 	}
		// 	$('.inner-link').smoothScroll({
		// 		speed: 900,
		// 		offset: header_offset
		// 	});

		// 	$('a.btn:not(.inner-link)').smoothScroll({
		// 		speed: 900,
		// 		offset: header_offset
		// 	});
		// });

		$(window).scroll(function() {
			bind_shrink_header();
		});

		function bind_shrink_header() {
			if ($('nav').hasClass('shrink-header')) {

				$('.shrink-header').addClass('shrink-nav');
				//$('section:first').addClass('header-margin-top');

			} else if ($('nav').hasClass('shrink-big-header')) {

				$('.shrink-big-header').addClass('shrink-nav');
				$('section:first').addClass('header-margin-top-big');

			} else if ($('nav').hasClass('shrink-medium-header')) {

				$('.shrink-medium-header').addClass('shrink-nav');
				$('section:first').addClass('header-margin-top-medium');

			} else if ($('nav').hasClass('shrink-transparent-header-dark')) {

				$('.shrink-transparent-header-dark').addClass('shrink-nav');

			} else if ($('nav').hasClass('shrink-transparent-header-light')) {

				$('.shrink-transparent-header-light').addClass('shrink-nav');

			} else {

				$('.shrink-header').removeClass('shrink-nav');
				//$('section:first').removeClass('header-margin-top');
			}

			if ($(window).scrollTop() > 20) {
				$('header nav.nav-ab').addClass('shrink');
			} else {
				$('header nav.nav-ab').removeClass('shrink');
			}

		}

		// if ($('.header nav').hasClass('shrink')) {
		// 	$('.header nav').parent().height($('.header nav').innerHeight());

		// }


		setTimeout(function() {
			$(window).scroll();
		}, 500);

	},



	SITE.masks = function() {

		$('.onlyN').keydown(function(e) {-1 !== $.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) || (/65|67|86|88/.test(e.keyCode) && (e.ctrlKey === true || e.metaKey === true)) && (!0 === e.ctrlKey || !0 === e.metaKey) || 35 <= e.keyCode && 40 >= e.keyCode || (e.shiftKey || 48 > e.keyCode || 57 < e.keyCode) && (96 > e.keyCode || 105 < e.keyCode) && e.preventDefault() });
		$('input.tel').inputmask("9999[9]-9999", { "placeholder": " ", "skipOptionalPartCharacter": " " });
		$('input.cpfcnpj').inputmask({mask: ['999.999.999-99', '99.999.999/9999-99'], keepStatic: true});
		$('input.mddd').inputmask("99", { "placeholder": " ", "clearIncomplete": true });
		$('input.mdate').inputmask("99/99/9999", { "placeholder": " ", "clearIncomplete": true });
		$('input.mcep').inputmask("99999-999", { "placeholder": " ", "clearIncomplete": true });
		$('input.mcnpj').inputmask("99.999.999/9999-99", { "placeholder": "", "clearIncomplete": true });
		$('input.mcpf').inputmask("999.999.999-99", { "placeholder": " ", "clearIncomplete": true });
		$('input.card').inputmask("999.999.999-99", { "placeholder": " ", "clearIncomplete": true });

		var i,x,y;
		jQuery.validator.addMethod("cpf", function(value, element) {
			value = jQuery.trim(value);
			value = value.replace('.', '');
			value = value.replace('.', '');
			cpf = value.replace('-', '');
			while (cpf.length < 11) cpf = "0" + cpf;
			var expReg = /^0+$|^1+$|^2+$|^3+$|^4+$|^5+$|^6+$|^7+$|^8+$|^9+$/;
			var a = [];
			var b = new Number;
			var c = 11;
			for (i = 0; i < 11; i++) { a[i] = cpf.charAt(i); if (i < 9) b += (a[i] * --c); }
			if ((x = b % 11) < 2) { a[9] = 0 } else { a[9] = 11 - x } b = 0;
			c = 11;
			for (y = 0; y < 10; y++) b += (a[y] * c--);
			if ((x = b % 11) < 2) { a[10] = 0; } else { a[10] = 11 - x; }
			var retorno = true;
			if ((cpf.charAt(9) != a[9]) || (cpf.charAt(10) != a[10]) || cpf.match(expReg)) retorno = false;
			return this.optional(element) || retorno;
		}, "CPF inválido");


		$('.numbersOnly').keypress(function(event){

			   if(event.which != 8 && isNaN(String.fromCharCode(event.which))){
				   event.preventDefault(); //stop character from entering input
			   }

		   });

		// $('#c_cep').on('blur', function() {

		// 	if ($.trim($("#c_cep").val()) != "") {

		// 		$("#c_endereco").html('Aguarde, estamos consultando seu CEP ...');
		// 		$.getScript("http://cep.republicavirtual.com.br/web_cep.php?formato=javascript&cep=" + $("#c_cep").val(), function() {

		// 			if (resultadoCEP["resultado"]) {
		// 				$("#c_endereco").val(unescape(resultadoCEP["tipo_logradouro"]) + " " + unescape(resultadoCEP["logradouro"])).focus();
		// 				$("#c_bairro").val(unescape(resultadoCEP["bairro"])).focus();
		// 				$("#c_cidade").val(unescape(resultadoCEP["cidade"])).focus();
		// 				$("#c_estado").val(unescape(resultadoCEP["uf"])).focus();
		// 			}
		// 			//$("#infocep").html('');
		// 		});
		// 	}
		// });


	},


	SITE.formLogin = function() {

		var $formLogin = $('#login__form');
		$formLogin.validate({
			rules: {
				login_email: {
					required: true,
					email: true
				},
				login_pass: {
					required: true,
					minlength: 3
				}
			},
			submitHandler: function() {

				$('#submit')
					.attr('disabled', 'disabled')
					.css('pointer-events', 'none')
					.html('Enviando...');

				var dataString = $formLogin.serialize();
				$.ajax({
					url: BASE + 'inicio/envia',
					type: 'POST',
					data: dataString,
					error: function(data) {

						$formLogin.find('#obrigadocontato').addClass('alert alert-danger').fadeIn().slideDown().html('POR FAVOR, TENTE NOVAMENTE!');
						$formLogin.find('#submit').removeAttr('disabled').html('ENVIAR');

					},
					success: function(data) {

						if (data.error) {

							$formLogin.find('#obrigadocontato').addClass('alert alert-danger').fadeIn().slideDown().html(data.error);
							$formLogin.find('#submit').removeAttr('disabled').css('pointer-events', 'inherit').html('ENVIAR');

						} else {

							$formLogin.find('#obrigadocontato').removeClass('alert-danger').addClass('alert alert-success').fadeIn().slideDown().html(data.success);

							$formLogin.find('#submit').removeAttr('disabled').css('pointer-events', 'inherit').html('ENVIAR');
							$formLogin.find('input[type=text],input[type=tel],input[type=email],textarea').val('');

						}


					}
				});
			},
			messages: {
				login_email: {
					required: "Informe um email válido"
				}
			},
			errorPlacement: function($error, $element) {
				var name = $element.attr("name");
				$("#error" + name).append($error);
				//$("#error" + name).parent('div').find('.input-effect').addClass('p-error');
			},
			highlight: function(element) {
				$(element).removeClass('valid');
				$(element).parent('div').addClass('p-error');
			},
			unhighlight: function(element) {
				$(element).addClass('valid');
				$(element).parent('div').removeClass('p-error');
			}
		});
	},


	// CADASTRO CUIDADOR
	SITE.formCadastroCuidador = function() {

		var $formCadastroCuidador = $('#cadastro__form');

		var validator = $formCadastroCuidador.validate({
			 ignore:":not(:visible)",
			 wrapper: "div",
			rules: {
				c_nome: { required: true, minlength: 3 },
				c_rg: { required: true, minlength: 5, digits: true },
				c_cpf: { cpf: true, required: true, minlength: 11 },
				c_nascimento: { required: true, minlength: 10 },
				c_cep: { required: true, minlength: 9 },
				c_endereco: { required: true },
				c_email: { required: true, email: true },
				c_celular: { required: true, minlength: 10 },
				c_refcelular1: { required: true, minlength: 10 }
			},
			submitHandler: function(form) {
				 if (! this.pendingRequest) {
					this.pendingRequest = true;
					form.submit();
				}
			},
			messages: {
				c_email: {
					required: "Informe um email válido"
				},
				c_cpf: {
					required: "Digite a senha"
				}
			},
			errorPlacement: function($error, $element) {
				var name = $element.attr("name");
				$("#error" + name).append($error);
				//$("#error" + name).parent('div').find('.input-effect').addClass('p-error');
			},
			highlight: function(element) {
				$(element).removeClass('valid');
				$(element).parent('div').addClass('p-error');
			},
			unhighlight: function(element) {
				$(element).addClass('valid');
				$(element).parent('div').removeClass('p-error');
			}
		});



		$formCadastroCuidador.find('#send').click(function() {
			if ($formCadastroCuidador.valid()){
				 $formCadastroCuidador.find('#send')
					.css('pointer-events','none')
					.html('ENVIANDO... <i class="fa fa-refresh fa-spin fa-fw" aria-hidden="true"></i>');
			} else
			  validator.focusInvalid();
		});


		$('input[name=c_mei]').click( function(){
			if ($(this).val()!=0) {
				$('#mei_cnpj').show();
			}else {
				$('#mei_cnpj').hide().find('.input-effect').removeClass('p-error');
			}
			//$('#mei_cnpj input').toggleClass('required');
		})


	},

	// CADASTRO NOVO USER
	SITE.formNovoCadastro = function() {

		var $formNCadastro = $('#novocadastro__form');

		var validator = $formNCadastro.validate({
			 wrapper: "div",
			 ignore: ":hidden",
			rules: {
				nc_nome: { required: true, minlength: 3 },
				nc_cpf: { cpf: true, required: true, minlength: 11 },
				nc_telefone: { required: true, minlength: 8 },
				nc_cep: { required: true, minlength: 9 },
				nc_endereco: { required: true },
				nc_email: { required: true, email: true },
				nc_senha: { required: true, minlength: 8  }
			},
			submitHandler: function(form) {
				 if (! this.pendingRequest) {
					this.pendingRequest = true;
					form.submit();
				}
			},
			messages: {
				c_email: {
					required: "Informe um email válido"
				},
				c_cpf: {
					required: "Digite a senha"
				}
			},
			errorPlacement: function($error, $element) {
				var name = $element.attr("name");
				$("#error" + name).append($error);
				//$("#error" + name).parent('div').find('.input-effect').addClass('p-error');
			},
			highlight: function(element) {
				$(element).removeClass('valid');
				$(element).parent('div').addClass('p-error');
			},
			unhighlight: function(element) {
				$(element).addClass('valid');
				$(element).parent('div').removeClass('p-error');
			}
		});



		$formNCadastro.find('#send').click(function() {
			if ($formNCadastro.valid()){
				 $formNCadastro.find('#send')
					.css('pointer-events','none')
					.html('ENVIANDO... <i class="fa fa-refresh fa-spin fa-fw" aria-hidden="true"></i>');
			} else
			  validator.focusInvalid();
		});


		$('input[name=nc_tipocadastro]').click( function(){
			if ($(this).val()!='fisica') {
				$('#pjuridica').removeClass('hidden').find('input').addClass('required');
				$('#pfisica').addClass('hidden').find('input').removeClass('required');
			}else {
				$('#pfisica').removeClass('hidden').find('input').addClass('required');
				$('#pjuridica').addClass('hidden').find('input').removeClass('required');
			}
		});

		$('input[name=nc_tipopagamento]').click( function(){
			if ($(this).val()!='credito') {
				$('#pboleto').removeClass('hidden').find('input').addClass('required');
				$('#pcredito').addClass('hidden').find('input').removeClass('required');
			}else {
				$('#pcredito').removeClass('hidden').find('input').addClass('required');
				$('#pboleto').addClass('hidden').find('input').removeClass('required');
			}
		})

	},



	SITE.formPagamento = function() {


		var $cardinput = $('#c_cardnumber');
		$('#c_cardnumber').validateCreditCard(function(result)
		{
			//console.log(result);
			if (result.card_type != null)
			{
				$cardinput.removeClassStartingWith('card_');
				switch (result.card_type.name)
				{
					case "visa":
						//$cardinput.css('background-position', 'right -27px!important');
						$cardinput.addClass('card_visa');
						break;

					case "visa_electron":
						//$cardinput.css('background-position', 'right -65px');
						$cardinput.addClass('card_visa_electron');
						break;

					case "mastercard":
						//$cardinput.css('background-position', 'right -103px');
						$cardinput.addClass('card_mastercard');
						break;

					case "maestro":
						//$cardinput.css('background-position', 'right -141px');
						$cardinput.addClass('card_maestro');
						break;

					case "discover":
						//$cardinput.css('background-position', 'right -179px');
						$cardinput.addClass('card_discover');
						break;

					case "amex":
						//$cardinput.css('background-position', 'right -216px');
						$cardinput.addClass('card_amex');
						break;
					default:
						$cardinput.css('background-position', 'right 10px');
						break;
				}
			} else {
				$cardinput.css('background-position', 'right 10px!important');
			}

			// Check for valid card numbere - only show validation checks for invalid Luhn when length is correct so as not to confuse user as they type.
			if (result.length_valid || $cardinput.val().length > 16)
			{
				if (result.luhn_valid) {
					$cardinput.parent().removeClass('has-error').addClass('has-success');
				} else {
					$cardinput.parent().removeClass('has-success').addClass('has-error');
				}
			} else {
				$cardinput.parent().removeClass('has-success').removeClass('has-error');
			}
		});



		var $formPagamento = $('#pagamento__form');
			window.$aceiteValidationCel = false;

		//Qdo botao submitar verifica se variavel [aceite] é true
		//SITE.formVerificacao() faz a validacao da variavel aceite
		//Caso form preenchido (valid) e [aceite](valid) submita o form!

		$('#submitPagamento').click(function(){

			$formPagamento.valid(); //forca validadao do form

			if ($formPagamento.valid() && !window.$aceiteValidationCel) {

				//abre modal para verificacao de [fone] e [aceite]
				var el = $('#cel-popup');
				$.magnificPopup.open({
					items: {
						src: el
					},
					type: 'inline',
					disableOn: 700,
					// modal:true,
					mainClass: 'mfp-fade',
					removalDelay: 160,
					preloader: false,
					focus: '#c_numeroconfirma',
					closeBtnInside: false,
					showCloseBtn: false,
					fixedContentPos: true,
					callbacks: {
						open: function() {
							SITE.formVerificacao();
						}
					}
				});

			} else {
				$formPagamento.submit();
			}



		});


		$formPagamento.validate({
			rules: {
				c_cpf: { cpf: true, required: true, minlength: 11 },
				c_email: { required: true, email: true },
				c_celular: { required: true, minlength: 10 },
				c_senha: { required: true, minlength: 5 },
				c_senhaagain: { required: true,  equalTo: "#c_senha"},
				c_cardnumber: { creditcard: true, minlength: 3, digits: true },
				c_cardmonth: { required: true, minlength: 2, digits: true },
				c_cardcvv: { required: true, minlength: 2, digits: true },
			},
			submitHandler: function(form){
				form.submit();
			},
			messages: {
				c_cardnumber: {
					required: "Digite o número do cartão"
				},
			},
			errorPlacement: function($error, $element) {
				var name = $element.attr("name");
				$("#error" + name).append($error);
				//$("#error" + name).parent('div').find('.input-effect').addClass('p-error');
			},
			highlight: function(element) {
				$(element).removeClass('valid');
				$(element).parent('div').addClass('p-error');
			},
			unhighlight: function(element) {
				$(element).addClass('valid');
				$(element).parent('div').removeClass('p-error');
			}
		});


	},


	SITE.formFluxo = function() {

		var current = 0,current_step,next_step,steps;
		steps = $("fieldset").length;
		setProgressBar(current);

		// Change progress bar action
		function setProgressBar(curStep){
			var percent = parseFloat(100 / steps) * curStep;
			percent = percent.toFixed();
			$(".progress-bar").css("width",percent+"%");
		}

		jQuery.validator.addMethod("cep", function(value, element) {
		  return this.optional(element) || /^[4]{1}[0-9]{1}[0-9]{3}-[0-9]{3}$/.test(value);
		}, "Desculpe, no momento a nova. ainda não está disponível para sua cidade. Nós avisaremos quando chegarmos.");

		var $formFluxo = $('#fluxo__form').show(),
		stepTitle,
		stepCount,
		stepAmbiente,
		stepQuartos,
		stepBanheiros,
		stepCBanheiros, //company
		stepPBanheiros, //party
		stepCMetros, //company
		stepPMetros, //party
		stepHoras,
		stepHorasaMais,
		stepServicos,
		stepFrequencia,
		stepData,
		stepHorario,
		chkArray = [], //Horas Servicos
		hours = $('._servico').data('inicial'), //Horas header
		selectedServices,
		calOptions;

		$formFluxo.steps({
			headerTag: "h3",
			bodyTag: "fieldset",
			//startIndex: 6,
			enableFinishButton: false,
			//saveState: true,

			onInit: function(event, currentIndex) {

				stepCount = $formFluxo.steps('getCurrentIndex')+1;

				if (stepCount > 1) {
					$('.actions').show();
				} else
				$('.actions').hide();

				if (stepCount == 5 ) {
				   $('#mdp-picker').multiDatesPicker({
						maxPicks: 2,
						pickableRange: 7
				});
				}


				//$('.navigation-menu .stepsContador span').html(stepCount);
				SITE.masks();

			},
			onStepChanging: function(event, currentIndex, newIndex) {

				$('html,body').scrollTop(0);

				// Allways allow previous action even if the current form is not valid!
				if (currentIndex > newIndex) {

					//decrease blue step progress
					setProgressBar(--current);

					//botoes de < >
					if (newIndex != 0 ) {
						$('.actions').show();
					} else
					$('.actions').hide();

					//destroy and clear dates if stepback - sign choose - in case changed
					//destroy and clear dates if stepback - sign choose - in case changed
					if (currentIndex===4) {
						$('#mdp-picker').multiDatesPicker('destroy'); //limpa calendario
						$('#mdp-picker').multiDatesPicker('removeIndexes', 0); //limpa calendario
					}

					return true;
				} else {

					if ($formFluxo.valid())
						setProgressBar(++current); //increase blue step progress

					//not if calendar day not chossed
					//not if calendar day not chossed
					//not if calendar day not chossed
					if (newIndex === 5){
					   if ($('input[name=frequencia]:checked').val()=='Duas vezes por semana'){
					   		if ($('#mdp-picker').multiDatesPicker('getDates')[1]==undefined) {
								$('#errorc_calendario').html('<label class="error">Você precisa escolher 2 datas</label>');
								setProgressBar(--current);
								return false;
							}
						} else if ($('#mdp-picker').multiDatesPicker('getDates')[0]==undefined) {
							$('#errorc_calendario').html('<label class="error">Você precisa escolher ao menos 1 data</label>');
							setProgressBar(--current);
							return false;
						}
					}


				}
				// Forbid next action on "Warning" step if the user is to young

				// Needed in some cases if the user went back (clean up)
				if (currentIndex < newIndex) {
					// To remove error styles
					$formFluxo.find(".body:eq(" + newIndex + ") label.error").remove();
					$formFluxo.find(".body:eq(" + newIndex + ") .error").removeClass("error");
				}
				$formFluxo.validate().settings.ignore = ":disabled,:hidden";

				//valores de metragens aqui (:visible só pega no -onChanging-)
				stepCMetros = $('input[name=c_cmetros]').val(); //metros2 company
				stepPMetros = $('input[name=c_pmetros]').val(); //metros2 party

				return $formFluxo.valid();
			},
			onStepChanged: function(event, currentIndex, priorIndex) {


				stepCount = currentIndex+1;

				//ativa calendario
				//ativa calendario
				//ativa calendario
				if (stepCount == 5 ) {

					var date = new Date();
				    var dayNo = date.getDay();
				    var mindate = (7-dayNo);

					//altera funcao do calendario pra selecionar uma data ou 2
					if ($('input[name=frequencia]:checked').val()=='Duas vezes por semana') {
						calOptions = {
							maxPicks: 2,
			                // firstDay: 1,
			               // maxDate: +7,
			                pickableRange: 7,
	adjustRangeToDisabled: true,
			                minDate: 1,
							beforeShowDay: function(date) {
					        	var day = date.getDay();
					       		return [(day != 0), ''];
					    	}
						}
					} else if ($('input[name=frequencia]:checked').val()=='Uma vez por semana') {

						calOptions = {
								beforeShowDay: function(date) {
						        	var day = date.getDay();
						        	return [(day != 0), ''];
						   		},
								maxPicks: 1
						}

					} else{

						calOptions = {
							minDate: 0,
							beforeShowDay: function(date) {
						        var day = date.getDay();
						        return [(day != 0), ''];
						    },
							maxPicks: 1,
							pickableRange: null
						}
					}


					$('#mdp-picker').multiDatesPicker(calOptions);


					//troca descricoes abaixo do titulo do calendario (data assinatura/data servico)
					//troca descricoes abaixo do titulo do calendario (data assinatura/data servico)
					//troca descricoes abaixo do titulo do calendario (data assinatura/data servico)
					if ($('input[name=frequencia]:checked').val()=='Apenas uma vez') {
						$('#c_freqco_sign,#c_freqco_twod').addClass('hidden');
						$('#c_freqco_ontime').removeClass('hidden');
					} else{
						$('#c_freqco_sign').removeClass('hidden');
						$('#c_freqco_ontime,#c_freqco_twod').addClass('hidden');

						if ($('input[name=frequencia]:checked').val()=='Duas vezes por semana') {
							$('#c_freqco_twod').toggleClass('hidden');
						}
					}

				}

				//se for setp2 mostra next/prev
				//se for setp2 mostra next/prev
				//se for setp2 mostra next/prev
				if (currentIndex === 1) {
					$('#incluso').removeClass('hidden');
					$('.actions').show();
					$('ul.nav-fluxo li:first-child').addClass('hidden');
					$('ul.nav-fluxo li:nth-child(2),ul.nav-fluxo li:nth-child(3)').removeClass('hidden');
				} else if(currentIndex === 0){
					$('#incluso').addClass('hidden');
					$('ul.nav-fluxo li:first-child').removeClass('hidden');
					$('ul.nav-fluxo li:nth-child(2),ul.nav-fluxo li:nth-child(3)').addClass('hidden');
				}


				// console.log(currentIndex);

				if (currentIndex === 2) {

				} else if (currentIndex === 3) {
					//if (stepHoras!='') $('._servico').html( (parseInt($('._servico').text())+stepHoras) );
				} else if (currentIndex === 4) {
					//$('.stepsChoosed ._frequencia').html(stepFrequencia+', ');
				} else if (currentIndex === 5) { //ultimo step adiciona alguns valores a campos hidden


					$('input[name=dataAgendada]').val($('#mdp-picker').multiDatesPicker('getDates'));
					$('input[name=formEscolhas]').val($('.stepsChoosed').html());

					if (!$('#finishbtn')[0]){
						$('#fluxo__form .actions ul').append('<li id="finishbtn"><button type="submit" id="submit" class="btn-large btn-circle icon btn bg-light-blue text-white"><span class="fa fa-angle-right"></span></button></li>');
					}
				}

				if (currentIndex > 3 && currentIndex < 5) {
					if ($('#finishbtn')[0]){
						$('#fluxo__form .actions li#finishbtn').remove();
					}
				}


			},
			onFinishing: function(event, currentIndex) {
				//console.log(currentIndex);
				$formFluxo.validate().settings.ignore = ":disabled";
				return $formFluxo.valid();

			},
			onFinished: function(event, currentIndex) {

				 $formFluxo.submit();

			},
			labels: {
				cancel: "Cancel",
				current: "current step:",
				pagination: "Pagination",
				finish: "<i class='fa fa-angle-right'></i>",
				next: "<i class='fa fa-angle-right'></i>",
				previous: "<i class='fa fa-angle-left'></i>",
				loading: "Loading ..."
			}
			}).validate({
				rules: {
					c_cep: { required: true, cep: true },
				},
				errorPlacement: function($error, $element) {
					var name = $element.attr("name");
					$("#error" + name).append($error);
					//$("#error" + name).parent('div').find('.input-effect').addClass('p-error');
				},
				highlight: function(element) {
					$(element).removeClass('valid');
					$(element).parent('div').addClass('p-error');
				},
				unhighlight: function(element) {
					$(element).addClass('valid');
					$(element).parent('div').removeClass('p-error');
				}
			});

			//Selecting type
			//Selecting type
			//Selecting type
			$('input[name="ambiente"]').on('change', function() {
				//console.log($('input[name=ambiente]:checked').val());
				if ($('input[name=ambiente]:checked').data('title')=='empresa') {
					$('#for_company').removeClass('hidden');
					$('#casa_options').addClass('hidden');
					$('#horas_options').removeClass('hidden');
					$('#for_house').addClass('hidden');
					$('#for_party').addClass('hidden');
					$('#incluso').attr('href','#inclusoempresa');
					$('input[name*=servico]:checked').trigger('click'); //clean house services
				} else if ($('input[name=ambiente]:checked').data('title')=='festa') {
					$('#for_party').removeClass('hidden');
					$('#for_company').addClass('hidden');
					$('#for_house').addClass('hidden');
					$('#casa_options').addClass('hidden');
					$('#horas_options').removeClass('hidden');
					$('#incluso').attr('href','#inclusofesta');
					$('input[name*=servico]:checked').trigger('click'); //clean house services
					var hamaiszero = false; var hamenoszero = false;
				} else {
					$('#for_house').removeClass('hidden');
					$('#for_company').addClass('hidden');
					$('#for_party').addClass('hidden');
					$('#casa_options').removeClass('hidden');
					$('#horas_options').addClass('hidden');
					$('#incluso').attr('href','#inclusocasa');
					$('._servico').html($('._servico').html()-$('input[name=c_horasamais]').val());
					$('input[name=c_horasamais]').val('0')
					$('.stepsChoosed ._servicos ._hrsamais').val('');
					$('._servico').html($('._servico').data('inicial'));
					hours = $('._servico').data('inicial');

				}

			});

			$('#removeDisabledInputs_1').click( function() {
				$('#endereco_section').find('input:not([type=image],[type=button],[type=submit])').removeAttr('disabled');
				$('#endereco_section').find('input:not([type=image],[type=button],[type=submit]):first').focus().select();
			});

			$('#removeDisabledInputs_2').click( function() {
				$('#cartao_section').find('input:not([type=image],[type=button],[type=submit])').removeAttr('disabled');
				$('#cartao_section').find('input:not([type=image],[type=button],[type=submit]):first').focus().select();
			});

			//increment metros quadrados
			//increment metros quadrados
			//increment metros quadrados
			$('.empresa-metros').paginathing({
				perPage: 1,
				insertAfter: '.empresa-metros',
				prevNext: true,
				limitPagination: 4,
				disabledClass: 'disabled',
				firstLast: false,
				prevText: '<i class="fa fa-minus"></i>',
				nextText: '<i class="fa fa-plus"></i>',
				containerClass: 'pagination-spinner',
				ulClass: 'pag-spinner'
			});
			$('.party-metros').paginathing({
				perPage: 1,
				insertAfter: '.party-metros',
				prevNext: true,
				limitPagination: 4,
				disabledClass: 'disabled',
				firstLast: false,
				prevText: '<i class="fa fa-minus"></i>',
				nextText: '<i class="fa fa-plus"></i>',
				containerClass: 'pagination-spinner',
				ulClass: 'pag-spinner'
			});


			//open sidebar details choosed items
			//open sidebar details choosed items
			//open sidebar details choosed items
			$('.openStepsChoosed').click( function() {

				$('.stepsChoosed').toggleClass('in')
				$('.plus-nav-steps').toggleClass('in');

			});


			//SERIALIZE
			//SERIALIZE
			//SERIALIZE
			$('#fluxo__form').find(':input').on('click change', function(e) {
				e.stopPropagation(); //<-- has no effect to the described behavior

				var $list;

			   // console.log($('#fluxo__form').not($disxabled_list).serializeArray());
				if ($('input[name=ambiente]:checked').data('title')=='empresa')
					//$list = $formFluxo.find(':input,#c_cep,input[name=ambiente],select[name=c_cmetros],input[name=c_cbanheiros]');
					$list = $formFluxo.find(':input').not('select[name=c_pmetros],input[name=c_pbanheiros],input[name=c_quartos],input[name=c_banheiros]');
				else if ($('input[name=ambiente]:checked').data('title')=='festa')
					$list = $formFluxo.find(':input').not('select[name=c_cmetros],input[name=c_cbanheiros],input[name=c_quartos],input[name=c_banheiros]');
				else
					$list = $formFluxo.find(':input').not('select[name=c_cmetros],input[name=c_cbanheiros],select[name=c_pmetros],input[name=c_pbanheiros]');


				console.log($list.serializeArray());

				//sidebar choosed
				stepAmbiente = $('input[name=ambiente]:checked').val(); //ambiente label
				stepQuartos = $('input[name=c_quartos]').val(); //quartos casa
				stepBanheiros = $('input[name=c_banheiros]').val(); //banheiros casa
				stepCMetros = $('select[name=c_cmetros]').val(); //banheiros para company
				stepCBanheiros = $('input[name=c_cbanheiros]').val(); //banheiros para company
				stepPMetros = $('select[name=c_pmetros]').val(); //banheiros para company
				stepPBanheiros = $('input[name=c_pbanheiros]').val(); //banheiros para company
				stepHorasaMais = $('input[name=c_horasamais]').val(); //qtas hrs a Mais
				stepFrequencia = $('input[name=frequencia]:checked').val();
				stepData = $('#mdp-picker').multiDatesPicker('getDates');
				stepHorario = $('input[name=horario]:checked').val();

				if ($('input[name=ambiente]:checked').data('title')=='casa') {
					$('.stepsChoosed ._quartos').show().val(stepQuartos+' quarto(s), '+stepBanheiros+' banheiro(s), ');
					$('.stepsChoosed ._banheiros').empty().hide();
				}else if ($('input[name=ambiente]:checked').data('title')=='empresa') {
					$('.stepsChoosed ._banheiros').show().val(stepCMetros+', '+stepCBanheiros+' banheiro(s), ');
					$('.stepsChoosed ._quartos').hide();
				}else {
					$('.stepsChoosed ._banheiros').show().val(stepPMetros+', '+stepPBanheiros+' banheiro(s), ');
					$('.stepsChoosed ._quartos').hide();
				}

				stepData = (stepData[0]) ? stepData[0] : '';


				if (stepData){
					$('.stepsChoosed .c_inicio').css('display','block');
					$('.stepsChoosed ._dates').val(stepData);
				}
				$('.stepsChoosed ._frequencia').val(stepFrequencia);
				$('.stepsChoosed ._horario').val(stepHorario);
				$('.stepsChoosed ._ambiente').val(stepAmbiente);


			});


			//qdo escolhe servicos altera horas no header
			//qdo escolhe servicos altera horas no header
			//qdo escolhe servicos altera horas no header
			$('input[name*=servico]').click(function() {

				var item = $(this).data('item')

				if ($(this).is(":checked")) {
					hours += parseFloat($(this).data('horas'));
				}else{
					hours -= parseFloat($(this).data('horas'));
				}

				//Adiciona servicos na Sidebar
				//Adiciona servicos na Sidebar
				//Adiciona servicos na Sidebar
				$('.stepsChoosed ._servicos').find('li:nth-child('+(item+1)+')').toggleClass('hidden');
				//Altera no header
				$('._servico').html(hours);

				//mostra titulo ADICIONAL na sidebar caso algum selecionado
				if ($('input[name*=servico]:checked').length)
					$('#servAdtTitle').removeClass('hidden');
				else
					$('#servAdtTitle').addClass('hidden');
			});

			//Horas a mais ou menos em Empresa e Festa
			//Horas a mais ou menos em Empresa e Festa
			//Horas a mais ou menos em Empresa e Festa
			var hamaiszero = false; var hamenoszero = false;
			$('#hrsamais').click(function() {

				//Adiciona na Sidebar
				if (stepHorasaMais<7 && !hamaiszero){
					hours++;
					//Altera no header
					$('._servico').html(hours);
					$('.stepsChoosed ._servicos ._hrsamais').val('+ '+ stepHorasaMais +' hora(s) adicionais');
					if (stepHorasaMais==6) hamaiszero = true; hamenoszero = false;
				}

			});
			$('#hrsamenos').click(function() {
				//Adiciona na Sidebar
				if (stepHorasaMais>=0 && !hamenoszero) {
					hours--;
					//Altera no header
					$('._servico').html(hours);
					$('.stepsChoosed ._servicos ._hrsamais').val('+ '+ stepHorasaMais +' hora(s) adicionais');
					if (stepHorasaMais==0) hamenoszero = true; hamaiszero = false;
				} else if (stepHorasaMais==0) {
				} else{
					$('.stepsChoosed ._servicos ._hrsamais').val('');
				}
			});


	},


	SITE.formVerificacao = function() {

		var $formV1 = $('#verificacao__s1form');
		var $formV2 = $('#verificacao__s2form');

		$formV1.validate({
			rules: {
				c_numeroconfirma: { required: true, minlength: 10 },
			},
			submitHandler: function() {

				$formV1.find('#submitv1')
				.css('pointer-events','none')
				.html('ENVIANDO <i class="fa fa-refresh fa-spin fa-fw" aria-hidden="true"></i>');

				setTimeout(function() {
					$('.step1').addClass('display-none');
					$('.step2').removeClass('display-none');
				}, 2000);
			},
			errorPlacement: function($error, $element) {
				var name = $element.attr("name");
				$("#error" + name).append($error);
				//$("#error" + name).parent('div').find('.input-effect').addClass('p-error');
			}
		});

		$formV2.validate({
			rules: {
				c_codconfirma: { required: true, minlength: 4 },
			},
			submitHandler: function() {

				$formV2.find('#submitv2')
				.css('pointer-events','none')
				.html('AUTENTICANDO <i class="fa fa-refresh fa-spin fa-fw" aria-hidden="true"></i>');

				setTimeout(function() {
					$('.step2').addClass('display-none');
					$('.step3').removeClass('display-none');
				}, 2000);

				$('#aceitaTermos').click( function(){
					window.$aceiteValidationCel = true;
					$('#pagamento__form').submit();
				})
			},
			errorPlacement: function($error, $element) {
				var name = $element.attr("name");
				$("#error" + name).append($error);
				//$("#error" + name).parent('div').find('.input-effect').addClass('p-error');
			}
		});

	},


	SITE.effects = function() {

		$(".input-effect input").focusout(function() {
			if ($(this).val() != "") {
				$(this).addClass("has-content");
			} else {
				$(this).removeClass("has-content");
			}
		});

		$('input:not([type=image],[type=button],[type=submit])').each( function(){
			if ($(this).val() != "") {
				$(this).addClass("has-content");
			}
		});

		$('.clone_field').on('click', function() {
			var old_name;
			var table = $($(this).parent());
			var row = $(this).parent().find('.form-group');
			var index = row.length;
			row = row.last().clone();
			//console.log(row);
			$('input, textarea', row).val('');
			$('select > option', row).prop('selected', false);
			$('input, textarea, select, checkbox, radio', row).each(function() {
				old_name = $(this).attr('name');
				$(this).attr('name', old_name.replace(/([a-zA-Z_0-9\[\]]+)\[([0-9]+)\](\[\])?/, '$1[' + index + ']$3'));
			});
			row.insertBefore($(this));
			row.find('.delete-field').removeClass('hidden');
			return false;
		});

		$(document).on('click', '.delete-field', function() {
			var item = $($(this).parent().parent());
			item.remove();
			return false;
		});

	},

	SITE.ajudaBusca = function() {

		function slugify(text){
		  // Use hash map for special characters
		  var specialChars = {"à":'a',"ä":'a',"á":'a',"â":'a',"æ":'a',"å":'a',"ë":'e',"è":'e',"é":'e', "ê":'e',"î":'i',"ï":'i',"ì":'i',"í":'i',"ò":'o',"ó":'o',"ö":'o',"ô":'o',"ø":'o',"ù":'o',"ú":'u',"ü":'u',"û":'u',"ñ":'n',"ç":'c',"ß":'s',"ÿ":'y',"œ":'o',"ŕ":'r',"ś":'s',"ń":'n',"ṕ":'p',"ẃ":'w',"ǵ":'g',"ǹ":'n',"ḿ":'m',"ǘ":'u',"ẍ":'x',"ź":'z',"ḧ":'h',"·":'-',"/":'-',"_":'-',",":'-',":":'-',";":'-'};

			return text.toString().toLowerCase()
			  .replace(/\s+/g, '-')           // Replace spaces with -
			  .replace(/&/g, '-and-')         // Replace & with 'and'
			  .replace(/[^\w\-]+/g, '')       // Remove all non-word chars
			  .replace(/\-\-+/g, '-')         // Replace multiple - with single -
			  .replace(/^-+/, '')             // Trim - from start of text
			  .replace(/-+$/, '');             // Trim - from end of text
		}

		$('#search__form').on('keyup keypress', function(e) {
		  var keyCode = e.keyCode || e.which;
		  if (keyCode === 13) {
			e.preventDefault();
			return false;
		  }
		});


		$('#autocompletee').devbridgeAutocomplete({
			serviceUrl: BASE+'/ajuda/busca/',
			"query": "Unit",
			onSelect: function (suggestion) {
				window.location = AJUDA+suggestion.tipo+'/'+slugify(suggestion.cat)+'/'+suggestion.data+'#answer';
			}
		});

	},


	SITE.popOver = function() {

		$('[data-toggle="popover"]').popover({
			trigger: 'hover',
			offset	: '100',
			container: 'body'
		}).on('shown.bs.popover',function(){

			if ( !$(this).data('content') ) {
				$(this).data('bs.popover').$tip.find('.popover-content').remove();
				$(this).data('bs.popover').$tip.find('.popover-title').css('padding','15px 0');
			}

		});

	}



	$(document).ready(function() {

		SITE.init();
		SITE.slideConfiar();
		SITE.discountForm();
		SITE.slideTestimonial();
		SITE.medias();
		SITE.menuHeader();
		SITE.masks();
		SITE.formLogin();
		SITE.formCadastroCuidador();
		SITE.formNovoCadastro();
		SITE.formFluxo();
		SITE.formPagamento();
		SITE.ajudaBusca();
		SITE.effects();
		if (!device.mobile() || $(window).width() > 768) SITE.popOver();



		if (device.mobile() || $(window).width() <= 767) {
			SITE.mobileTitens();
		}



		$(window).resize(function() {
			setTimeout(function() {

				//set equalize height
				if (!device.mobile()) {
					$(window).unbind('equalize');
					$('.equalize').equalize({ reset: true });
				}
			}, 500);
		});

		$(window).on("orientationchange", function() {
			if (device.mobile()) {

				$(window).unbind('equalize');
				//$('.equalize > div').css('height', '');
				setTimeout(function() {
					$('.equalize').equalize();
				}, 500);
			}
		});


		// Hide Header on on scroll down
		var didScroll;
		var lastScrollTop = 0;
		var delta = 5;
		var navbarHeight = $('header nav').outerHeight();

		$(window).scroll(function(event){
			didScroll = true;
		});

		setInterval(function() {
			if (didScroll) {
				hasScrolled();
				didScroll = false;
			}
		}, 250);

		function hasScrolled() {
			var st = $(this).scrollTop();

			// Make sure they scroll more than delta
			if(Math.abs(lastScrollTop - st) <= delta)
				return;

			// If they scrolled down and are past the navbar, add class .nav-up.
			// This is necessary so you never see what is "behind" the navbar.
			if (st > lastScrollTop && st > navbarHeight){
				// Scroll Down
				$('header nav').removeClass('nav-down').addClass('nav-up');
			} else {
				// Scroll Up
				if(st + $(window).height() < $(document).height()) {
					$('header nav').removeClass('nav-up').addClass('nav-down');
				}
			}

			lastScrollTop = st;
		}


		// var wow = new WOW({
		// 	boxClass: 'wow',
		// 	animateClass: 'animated',
		// 	offset: 90,
		// 	mobile: false,
		// 	live: true
		// });
		// wow.init();



	});


});

